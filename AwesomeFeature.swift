
struct AwesomeFeature {

    // MARK: - Parameters

    private let awesomeParameter: String

    // MARK: - Intialiazers

    init(awesomeParameter: String) {
        self.awesomeParameter = awesomeParameter
    }

    // MARK: - Functions

    func printAwesomeParameter() {
        print(awesomeParameter)
    }
}
